<?php
/**
 * Entity manager for any entity
 *
 * @category EntityManager
 * @author   Romain Laneuville <romain.laneuville@hotmail.fr>
 */

namespace classes\entitiesManager;

use \abstracts\designPatterns\EntityManager as EntityManager;

/**
 * Performed database action relative to the any entity class
 *
 * @class GlobalEntityManager
 */
class GlobalEntityManager extends EntityManager
{
}
