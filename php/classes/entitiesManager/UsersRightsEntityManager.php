<?php
/**
 * Entity manager for the entity UsersRights
 *
 * @category EntityManager
 * @author   Romain Laneuville <romain.laneuville@hotmail.fr>
 */

namespace classes\entitiesManager;

use \abstracts\designPatterns\EntityManager as EntityManager;

/**
 * Performed database action relative to the UsersRights entity class
 *
 * @class UsersRightsManager
 */
class UsersRightsEntityManager extends EntityManager
{
}
